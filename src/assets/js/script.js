const menuBar = document.querySelector('.header__bars')
const menuNav = document.querySelector('.header__menu')

menuBar.addEventListener('click', () => {
    menuNav.classList.toggle('showMenu')
})

const slider = document.querySelector('.header__content')
const slide = document.querySelectorAll('.header__content--slide')
const bullets = document.querySelector('.header__bullets')
const bullet = document.querySelectorAll('.header__content--slide')

slider.firstElementChild.classList.add('slideAtivo')
bullets.firstElementChild.classList.add('bullet-ativo')

setInterval(() => {
    let sliderAtivo = document.querySelector('.slideAtivo')
    let bulletAtivo = document.querySelector('.bullet-ativo')

    if( sliderAtivo.dataset.id < slide.length ){
        sliderAtivo.classList.remove('slideAtivo')
        sliderAtivo.nextElementSibling.classList.add('slideAtivo')

        bulletAtivo.classList.remove('bullet-ativo')
        bulletAtivo.nextElementSibling.classList.add('bullet-ativo')
    } else{
        sliderAtivo.classList.remove('slideAtivo')
        slider.firstElementChild.classList.add('slideAtivo')
        
        bulletAtivo.classList.remove('bullet-ativo')
        bullets.firstElementChild.classList.add('bullet-ativo')
    }

}, 2000)